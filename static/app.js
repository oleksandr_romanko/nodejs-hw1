const App = {
  data: () => ({
        file: {
          filename:'',
          content: '',
          password: ''
        },
        files: [],
        fileInfo: {},
        toast: {
          message: '',
          status: 200
        }
  }),
  methods: {
    async createFile() {
      const res = await fetch('/api/files', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(this.file)
      })

      this.file.filename = ''
      this.file.content = ''
      const data = await res.json()
      this.toastShow(data.message, res.status)
    },

    async getFiles() {
          const res = await fetch('/api/files')
          const data = await res.json()
          this.files = data.files
          console.log(res)
          this.toastShow(data.message, res.status)
    },

    async getFileByName(filename) {
      const res = await fetch(`/api/files/${filename}`)
      this.fileInfo = await res.json()
    },

    async deleteFile(filename) {
      const res = await fetch(`/api/files/${filename}`, {method: 'DELETE'})
      this.files = this.files.filter(file => file !== filename)

      const { message } = await res.json()
      this.toastShow(message, res.status)
    },

    async editFile() {
      const res = await fetch(`/api/files/${this.fileInfo.filename}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(this.fileInfo)
      })

      const { message } = await res.json()
      this.toastShow(message, res.status)
    },

    toastShow(msg, status){
      this.toast.status =  status
      this.toast.message = msg
      return new bootstrap.Toast(document.querySelector('.toast')).show()
    }
  }
}

Vue.createApp(App).mount('#app')
