const fs = require('fs').promises;
const path = require('path');

const create = (req, res) => {
  const { filename, content } = req.body
  const dirPath = path.join(__dirname, '../files');
  const filePath = path.join(__dirname, '../files', filename);
  const extensions = ['.log', '.txt', '.json', '.yaml', '.xml', '.js']
  const filenameExtension = path.extname(filename)


  if(!filename || !content){
    return res.status(400).json({message: 'Required parameters were not passed'});
  }

  if (!extensions.includes(filenameExtension)) {
    return res.status(400).json({message: 'Please specify correct file extension'});
  }

  const createFile = (filePath, content) => {
    fs.writeFile(filePath, content)
        .then(() => res.status(200).json({message: 'File created successfully'}))
        .catch(() => res.status(500).json({message: 'Server error'}));
  }

  fs.access(dirPath, fs.F_OK)
    .then(() => createFile(filePath, content))
    .catch(() => {
      fs.mkdir('files')
        .then(() => createFile(filePath, content))
        .catch(() => res.status(500).json({message: 'Server error'}))
    })
}

const getAll = (req, res) => {
  const dirPath = path.join(__dirname, '../files');

  const getFiles = (dirPath) => {
    fs.readdir(dirPath)
      .then(result => {
        if(result.length){
          res.status(200).json({message: 'Success', files: result})
        } else {
          res.status(400).json({message: 'No files have found'})
        }
      })
      .catch(() => res.status(500).json({message: 'Server error'}));
  }

  fs.access(dirPath, fs.F_OK)
    .then(() => getFiles(dirPath))
    .catch(() => res.status(400).json({message: 'No files have found'}))
}

const getFilename = (req, res) => {
  const { filename } = req.params
  const filePath = path.join(__dirname, '../files', filename);

  if(!filename){
    return res.status(400).json({message: `Required parameter ${filename} were not passed`});
  }

  fs.access(filePath, fs.F_OK)
    .then(() => {
      const birthtime = fs.stat(filePath)
        .then(result => result.birthtime)
        .catch(() => res.status(500).json({message: 'Server error'}));

      const content = fs.readFile(filePath, 'utf8')
        .then(result => result)
        .catch(() => res.status(500).json({message: 'Server error'}));

      const extension = new Promise(resolve => {
          const ext = path.extname(filename||'').split('.').pop();
          resolve(ext);
        })

      Promise.all([birthtime, content, extension]).then((result) => {
        res.status(200).json({
            message: 'Success',
            filename: filename,
            content: result[1],
            extension: result[2],
            uploadedDate: result[0]
          });
        });
  })
  .catch(() => res.status(400).json({message: `No file with ${filename} filename found`}))

};

const deleteFile = (req, res) => {
  const { filename } = req.params
  const filePath = path.join(__dirname, '../files', filename);

  if(!filename){
    return res.status(400).json({message: `Required parameter ${filename} were not passed`});
  }

  fs.access(filePath, fs.F_OK)
    .then(() => {
      fs.unlink(filePath)
        .then(() => res.status(200).json({message: `${filename} successfully deleted`}))
        .catch(() => res.status(500).json({message: "Server error"}));
  })
  .catch(() => res.status(400).json({message: `No file with ${filename} filename found`}))
};

const editFile = (req, res) => {
  const {filename, content} = req.body
  const filePath = path.join(__dirname, '../files', filename);

  if(!filename || !content){
    return res.status(400).json({message: `Required parameters were not passed`});
  }

  fs.access(filePath, fs.F_OK)
    .then(() => {
      fs.writeFile(filePath, content)
        .then(() => res.status(200).json({message: 'File successfully modified'}))
        .catch(() => res.status(500).json({message: 'Server error'}));
  })
  .catch(() => res.status(400).json({message: `No file with ${filename} filename found`}))

}

module.exports = {
  getAll,
  create,
  getFilename,
  deleteFile,
  editFile
}
