# RD LAB FE NODEJS HOMEWORK №1

Use NodeJS to implement web-server which hosts and serves 
files.

# Requirements:
- Use standard http module or express framework to implement simple web-server;
- Use fs module to create/modify/read ﬁles in ﬁle system;
- Write every request info to logs;
- Application should support log, txt, json, yaml, xml, js ﬁle extensions ( consider ﬁlename may contain '.' symbol);
- Please check that your app can be launched with 'npm install' and 'npm start' commands just after git pull;
- Application should work at port 8080.


# Acceptance criteria:
- Server saves ﬁle on createFile request and responds with 200 status, use ‘ﬁlename' and ‘content’ body params to transfer ﬁle data.
- Server returns list of uploaded ﬁles on getFiles request
- Server returns ﬁle content on getFile request, use 'ﬁlename' url parameter to determine what ﬁle you want to retrieve.
- In case there are no ﬁle with provided name found, return 400 status.
- Gitlab repo link and project id are saved in Google spreadsheets by [link](https://docs.google.com/spreadsheets/d/1_PhsGuS_ucQO1WWaarGxdb6aYvAs6remqFLFHHVKCkY/edit?usp=sharing)


# Optional criteria:
- Server handles errors and validates input params for all requests. 
- You can add password protection for ﬁles, with additional(optional) parameter 'password'  in createFile request, and additional(optional) parameter ‘password' in getFile request in url query params. Consider server should handle invalid and empty password for protected ﬁles.
- Ability to modify ﬁles content (additional endpoint);
- Ability to delete ﬁles (additional endpoint);


