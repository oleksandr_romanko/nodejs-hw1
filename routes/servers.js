const { Router } = require('express');
const { getAll, create, getFilename, deleteFile, editFile } = require ('../controllers/servers.js');
const { setPassword, handlePassword, handleProtectedFiles } = require('../middlewares/fileProtection')
const router = Router();

router.get('/api/files',handleProtectedFiles, getAll);

router.post('/api/files',setPassword, create);

router.get('/api/files/:filename', handlePassword, getFilename);

router.delete('/api/files/:filename',handlePassword, deleteFile);

router.put('/api/files/:filename',handlePassword, editFile);

module.exports = router;
