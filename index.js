const express = require('express');
const morgan = require('morgan')
const path = require('path');
const fs = require('fs');
const serverRoutes = require('./routes/servers.js');

const PORT = process.env.PORT ?? 8080;
const app = express();
app.use(express.static(path.resolve(__dirname, 'static')));

const accessLogStream = fs.createWriteStream(path.join(__dirname,'access.log'), { flags: 'a' });

app.use(morgan('common', { stream: accessLogStream}));
app.use(express.json())
app.use(express.urlencoded({extended: false}))
app.use(serverRoutes);

app.get('/', (req, res) => {
  res.render('index')
})

app.listen(PORT, () => {
  console.log(`Server has been started on port ${PORT}...`);
});
