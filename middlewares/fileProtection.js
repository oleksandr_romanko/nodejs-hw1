const fs = require('fs')
const fsPromises = fs.promises;
const path = require('path');

const setPassword = async (req, res, next) => {
  const { filename, password } = req.body
  const passPath = path.join(__dirname, 'password.json');

  if (password) {
    if (password.length < 6 || !password.trim()) {
      return res.status(400).json({ message: 'Passowrd can not be empty or less than 6 symbols' })
    }

    const file = {
      filename,
      password
    }

    try {

      if (!fs.existsSync(passPath)) {
        await fsPromises.writeFile(passPath, `${JSON.stringify([file])}`, 'utf-8')
      } else {

        // array of protected files
        const passwordData = await fsPromises.readFile(passPath, 'utf-8')

        // Check if file is not empty
        if (passwordData) {
          const parsed = JSON.parse(passwordData)
          parsed.push(file)

          await fsPromises.writeFile(passPath, `${JSON.stringify(parsed)}`, 'utf-8')
        } else {
          await fsPromises.writeFile(passPath, `${JSON.stringify([file])}`, 'utf-8')
        }
      }

    } catch {
      return res.status(500).json({ message: 'Server error' })
    }
  }

  next()
}

const handleProtectedFiles = async (req, res, next) => {
  const dirPath = path.join(__dirname, '../files');

  if (!fs.existsSync(dirPath)) {
    return res.status(400).json({ message: `There are no files` })
  }

  try {
    const fileList = await fsPromises.readdir(dirPath)

    if (!fileList.length) {
      return res.status(400).json({ message: `There are no files` })
    }

    req.data = fileList

  } catch {
    res.status(400).json({ message: 'Server error' })
  }

  next()
}

const handlePassword = async (req, res, next) => {
  const password = req.query.password
  const filename = req.params.filename
  const passPath = path.join(__dirname, 'password.json');

  try {
    if (fs.existsSync(passPath)) {
      const passwordData = await fsPromises.readFile(passPath, 'utf-8')

      // Check if file is not empty
      if (passwordData) {
        const parsed = JSON.parse(passwordData)

        const currentFile = parsed.find((file) => {
          return file.filename === filename
        })

        if (currentFile) {
          if (currentFile.password !== password) {
            return res.status(400).json({ message: 'Password is not provided or invalid' })
          }
        }
      }
    }
  } catch {
    return res.status(500).json({ message: 'Server error' })
  }

  next()
}

module.exports = { setPassword, handlePassword, handleProtectedFiles }
